import datetime
from typing import Any, Union

import dash_bootstrap_components as dbc
import dash_cytoscape as cyto
from dash import callback, dcc, html
from dash.dependencies import Input, Output, State
from networkx import Graph

import app.components as cmp
import app.constants as cst
import lib.markdown_elements as mds
from lib import network, plots

ID = "institutions"

page_institutions_layout = dbc.Col(
    [
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Alert(
                        [
                            html.I(className="bi bi-exclamation-triangle"),
                            " Le travail sur les Structures est en cours et comporte des erreurs ",
                            html.I(className="bi bi-exclamation-triangle"),
                        ],
                        color="primary",
                    ),
                    width={"size": 6},
                ),
                cmp.updated_when_badge,
            ]
        ),
        html.H2("Structures publiant sur le SNDS\t"),
        html.H5("Données et statistiques des Structures publiant des articles sur le SNDS", style={"font-style": "italic"}),
        cmp.card_generate_full_plots(
            title=[
                "Pays des Structures publiant sur le SNDS\t",
                cmp.info_icon("structure-map-countries-info"),
                cmp.popover("structure-map-countries-info", mds.MDS["affiliation_per_authors.md"]),
            ],
            id_="structure-map-countries",
        ),
        html.Br(),
        cmp.card_generate_full_plots(
            title=[
                "Villes des Structures publiant sur le SNDS\t",
                cmp.info_icon("author_cities_info"),
                cmp.popover("author_cities_info", mds.MDS["author_cities.md"]),
            ],
            id_="cities_per_author",
        ),
        html.Br(),
        cmp.card_generate_full_plots(
            title=[
                "Nombre d'articles par Structure\t",
                cmp.info_icon("affiliation_per_authors_info_badge_id"),
                cmp.popover("affiliation_per_authors_info_badge_id", mds.MDS["affiliation_per_authors.md"]),
            ],
            id_="affiliation_per_authors",
        ),
        html.Br(),
        html.A(id="struct-find"),
        dbc.Card(
            [
                dbc.CardHeader("Trouver une Structure"),
                dbc.CardBody(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        html.Label("Filtrer par Pays ", style={"font-weight": "bold"}),
                                        dcc.Dropdown(
                                            options=cst.countries,
                                            id="Country-select-dropdown",
                                            placeholder="Rechercher des Pays",
                                        ),
                                    ],
                                ),
                                dbc.Col(
                                    [
                                        html.Label("Filtrer par Ville ", style={"font-weight": "bold"}),
                                        dcc.Dropdown(
                                            options=cst.cities,
                                            id="City-select-dropdown",
                                            placeholder="Rechercher des Villes",
                                        ),
                                    ]
                                ),
                            ]
                        ),
                        html.Br(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    dbc.RadioItems(
                                        id=f"{ID}-and-or",
                                        options=[
                                            {"label": "AND", "value": 0},
                                            {"label": "OR", "value": 1},
                                        ],
                                        value=0,
                                    ),
                                    width={"size": "1"},
                                    align="end",
                                ),
                                dbc.Col(
                                    [
                                        html.Label(
                                            [
                                                "Filtrer par MeSH ",
                                                cmp.info_icon("filter-mesh-badge"),
                                                cmp.popover("filter-mesh-badge", mds.MDS["mesh_filter_text.md"]),
                                            ],
                                            style={"font-weight": "bold"},
                                        ),
                                        dcc.Dropdown(
                                            options=cst.raw_meshs.FullMeSH,
                                            id="MeSH-select-dropdown",
                                            multi=True,
                                            placeholder="Rechercher des MeSH",
                                        ),
                                    ],
                                ),
                                dbc.Col(
                                    [
                                        html.Label(
                                            [
                                                "Filtrer par Mots-Clefs ",
                                                cmp.info_icon("filter-keywords-badge"),
                                                cmp.popover("filter-keywords-badge", mds.MDS["keywords_filter_text.md"]),
                                            ],
                                            style={"font-weight": "bold"},
                                        ),
                                        dcc.Dropdown(
                                            options=cst.raw_keywords.NormalizedKeywords,
                                            id="Keywords-select-dropdown",
                                            multi=True,
                                            placeholder="Rechercher des Mots-Clefs",
                                        ),
                                    ],
                                ),
                            ],
                            justify="end",
                        ),
                        html.Hr(),
                        dbc.Row(
                            dcc.Dropdown(
                                options=cst.ror_affiliations.ror,
                                id="structure-select-dropdown",
                                multi=False,
                                placeholder="Rechercher une Structure",
                            ),
                        ),
                    ]
                ),
            ]
        ),
        html.Br(),
        dbc.Card(
            [
                dbc.CardHeader(
                    html.H4(
                        cst.SELECTED_ROR,
                        id="structure-card-title",
                        style={"font-weight": "bold", "color": "#06303A"},
                    ),
                    style={
                        "background-color": "#5DEEE3",
                    },
                ),
                dbc.CardBody(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="structure-nb-publi", style=cst.style_numbers),
                                            html.H6("Publication(s) sur le SNDS dans Pubmed", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[0],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="structure-nb-authors", style=cst.style_numbers),
                                            html.H6("Auteur(s) unique", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[2],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="structure-sum-cit", style=cst.style_numbers),
                                            html.H6("Citation(s) total", style=cst.style_text),
                                            html.Small(
                                                "*Total calculé sur les articles SNDS au moment de la récupération des données",
                                                style=cst.style_small_text,
                                            ),
                                        ],
                                        style=cst.style_cards[1],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="structure-last-publi", style=cst.style_numbers),
                                            html.H6("Dernière publication sur le SNDS", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[3],
                                        body=True,
                                    )
                                ),
                            ],
                        ),
                        html.A(id="struct-graph-links"),
                        dbc.Card(
                            [
                                dbc.CardHeader(
                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                html.H5(id="structure-graph-card-hearder"),
                                                width={"size": 9},
                                            ),
                                            dbc.Col(
                                                cmp.download_button("structures-download-graph-button"),
                                                width={"order": "last"},
                                            ),
                                        ],
                                        justify="between",
                                    ),
                                ),
                                dbc.CardBody(
                                    [
                                        cyto.Cytoscape(
                                            id="structure-graph",
                                            layout=cst.cyto_layout_inst,
                                            style=cst.cyto_style,
                                            elements=cst.graph_data["structures"].sub_graph,
                                            autounselectify=True,
                                            stylesheet=cst.cyto_stylesheet,
                                            maxZoom=2,
                                            minZoom=0.3,
                                        ),
                                        html.Hr(),
                                        dbc.Row(
                                            [
                                                dbc.Col(
                                                    dbc.Button("Reset", id="structure-graph-reset"),
                                                    width={"size": 1},
                                                ),
                                                dbc.Col(
                                                    [
                                                        dbc.Input(
                                                            id="structure-depth-limit",
                                                            type="number",
                                                            min=1,
                                                            max=5,
                                                            value=1,
                                                        ),
                                                        dbc.FormText("Profondeur"),
                                                    ],
                                                    width={"size": 2},
                                                ),
                                            ],
                                        ),
                                        dbc.Accordion(
                                            dbc.AccordionItem(
                                                cmp.generate_dashtable_element(
                                                    id_="structure-graph-tapnode-out",
                                                    columns=cst.ror_institutions_columns,
                                                    data=cst.front_ror_affiliations.loc[
                                                        cst.front_ror_affiliations["Structure"].str.contains(cst.SELECTED_ROR)
                                                    ].to_dict(
                                                        "records"
                                                    ),  # type: ignore
                                                    fixed_rows={"headers": False},
                                                    style_table={
                                                        "maxWidth": "100%",
                                                        "overflowY": "scroll",
                                                    },
                                                ),
                                                title="Info du Noeud Selectionné",
                                            )
                                        ),
                                    ],
                                ),
                            ]
                        ),
                        html.Br(),
                        html.A(id="struct-publications"),
                        dbc.Row(
                            dbc.Col(
                                [
                                    dbc.Card(
                                        [
                                            dbc.CardHeader(html.H5("Publications")),
                                            dbc.CardBody(
                                                cmp.generate_dashtable_element(
                                                    id_="structure-publications-table",
                                                    columns=cst.publications_columns,
                                                    data=cst.frontend_publications.loc[
                                                        cst.frontend_publications.doi.isin(
                                                            cst.ror_affiliations.loc[cst.ror_affiliations.ror == cst.SELECTED_ROR].doi.tolist()
                                                        )
                                                    ].to_dict(
                                                        "records"
                                                    ),  # type: ignore
                                                    style_table={"height": "auto", "maxWidth": "100%", "overflowY": "scroll"},
                                                    page_size=10,
                                                ),
                                            ),
                                        ],
                                    ),
                                ]
                            ),
                        ),
                        html.Br(),
                        html.A(id="struct-mesh-keywords"),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardHeader(html.H5("Mots-Clés")),
                                                dbc.CardBody(
                                                    cmp.generate_dashtable_element(
                                                        id_="structure-table-Keywords",
                                                        columns=[{"name": i, "id": i} for i in ["Mots-Clés", "Compte"]],
                                                        data=[],
                                                        style_table={},
                                                        style_cell={"font-family": "'Montserrat', sans-serif"},
                                                        fixed_rows={"headers": False},
                                                        page_size=17,
                                                    ),
                                                ),
                                            ],
                                        ),
                                    ]
                                ),
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardHeader(html.H5("MeSH (Medical Subject Headings)")),
                                                dbc.CardBody(
                                                    cmp.generate_dashtable_element(
                                                        id_="structure-table-MeSH",
                                                        columns=[{"name": i, "id": i} for i in ["MeSH", "Compte"]],
                                                        data=[],
                                                        style_table={},
                                                        style_cell={"font-family": "'Montserrat', sans-serif"},
                                                        fixed_rows={"headers": False},
                                                        page_size=17,
                                                    ),
                                                ),
                                            ],
                                        ),
                                    ],
                                ),
                            ]
                        ),
                    ]
                ),
            ],
            outline=True,
        ),
        # Legal
        html.Br(),
        html.Br(),
        cmp.end_page_accordion,
        html.P(id="inv_timestampbis"),
    ],
    class_name="page-loader",
)


@callback(
    Output("structure-map-countries", "figure"),
    Output("affiliation_per_authors", "figure"),
    Output("cities_per_author", "figure"),
    Input("inv_timestampbis", "loading_state"),
)
def get_plots(_):
    """callback creates and returns particle db pie

    Returns:
        tuple: a tuple with 3 plots
    """
    return (
        plots.plot_map_world_structure_publications(),
        plots.plot_publications_per_institution(),
        plots.plot_cities_of_structures_per_article(),
    )


@callback(
    Output("structure-publications-table", "data"),
    Input("structure-select-dropdown", "value"),
)
def update_structure_publications(name: str) -> Union[list[dict[str, Any]], dict[Any, Any]]:
    """callback updates article list of selected structure

    Args:
        name (str): structure name

    Returns:
        Union[list[dict[str, Any]], dict[Any, Any]]: publications of selected author
    """
    if not name:
        name = cst.SELECTED_ROR
    dois = cst.ror_affiliations.loc[cst.ror_affiliations.ror == name].doi.tolist()[0]
    return cst.frontend_publications.loc[cst.frontend_publications.doi.isin(dois)].to_dict("records")


@callback(
    Output("structure-select-dropdown", "options"),
    Input("MeSH-select-dropdown", "value"),
    Input("Keywords-select-dropdown", "value"),
    Input("Country-select-dropdown", "value"),
    Input("City-select-dropdown", "value"),
    Input(f"{ID}-and-or", "value"),
)
def update_data_author(
    meshs: list[str],
    keywords: list[str],
    country: str,
    city: str,
    or_: bool,
) -> dict[str, Any]:
    """callback updates main author table

    Args:
        meshs (list[str]): mesh list from mesh dropdown menu item
        keywords (list[str]): keyword list from keyword dropdown menu item
        country (str): country from country dropdown menu item
        city (str): city from city dropdown menu item
    Returns:
        list[dict[str, Any]]: filtered dataframe in to_dict("records") form
    """
    explode_affs = cst.ror_affiliations.explode("doi")
    subset = cst.ror_affiliations
    if meshs:
        grouped = cst.raw_meshs.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in meshs:
                ldois += grouped.doi[grouped.FullMeSH.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.FullMeSH.map(set(meshs).issubset)].tolist()
        names = explode_affs.loc[explode_affs.doi.isin(dois)].ror
        subset = subset.loc[subset.ror.isin(names)]
    if keywords:
        grouped = cst.raw_keywords.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in keywords:
                ldois += grouped.doi[grouped.NormalizedKeywords.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.NormalizedKeywords.map(set(keywords).issubset)].tolist()
        names = explode_affs.loc[explode_affs.doi.isin(dois)].FullName
        subset = subset.loc[subset.ror.isin(names)]
    if country:
        subset = subset.loc[subset.country_refactored == country]
    if city:
        subset = subset.loc[subset.city == city]
    return subset.ror


@callback(
    Output("inv_timestampbis", "children"),
    Input("structure-table", "active_cell"),
)
def update_table_click_timestamp(_) -> None:
    """updates timestamp for table click

    Args:
        _ (unused): unused
    """
    cst.STRUCTURE_TABLE_CLICK_TIMESTAMP = int(datetime.datetime.now().timestamp() * 1000)


# Stat Cards
@callback(
    Output("structure-card-title", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_main_card_title(name: str) -> str:
    """updates main card title

    Args:
        name (int): name of selected ror

    Returns:
        str: the new structure name
    """
    if not name:
        name = cst.SELECTED_ROR
    return name


@callback(
    Output("structure-nb-publi", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_nb_publi_structure(name) -> str:
    """callback updates number publications of selected structure

    Args:
        name (str): structure name

    Returns:
        str: number of publications
    """
    if not name:
        name = cst.SELECTED_ROR
    return f"{len(cst.ror_affiliations.loc[cst.ror_affiliations.ror == name]['doi'].to_list()[0])}"


@callback(
    Output("structure-nb-authors", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_nb_authors(name) -> str:
    """callback updates mean coauthor of selected structure

    Args:
        name (str): structure name

    Returns:
        str: nb authors of selected structure
    """
    if not name:
        name = cst.SELECTED_ROR
    return f"{len(cst.ror_affiliations.loc[cst.ror_affiliations.ror == name].FullName.to_list()[0])}"


@callback(
    Output("structure-sum-cit", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_sum_cit_structure(name) -> str:
    """callback updates total citations of selected structure

    Args:
        name (str): structure name

    Returns:
        str: total citations of selected structure
    """
    if not name:
        name = cst.SELECTED_ROR
    articles = cst.ror_affiliations.loc[cst.ror_affiliations.ror == name]["doi"].item()
    return f"{int(sum(cst.publications.loc[cst.publications.doi.isin(articles)]['PmcRefCount']))}*"


@callback(
    Output("structure-last-publi", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_last_publi_structure(name) -> str:
    """callback updates last publication date of selected structure

    Args:
        name (str): structure name

    Returns:
        str: last publication date of selected structure
    """
    if not name:
        name = cst.SELECTED_ROR
    articles = cst.ror_affiliations.loc[cst.ror_affiliations.ror == name]["doi"].item()
    list_publi_dates = cst.publications.loc[cst.publications.doi.isin(articles)]["PubDate"].to_list()
    list_publi_dates.sort()
    return f"{list_publi_dates[-1]}"


# MeSH & Keywords
@callback(
    Output("structure-table-MeSH", "data"),
    Input("structure-select-dropdown", "value"),
)
def update_mesh_table(name) -> dict[str, Any]:
    """updates mesh table of selected structure

    Args:
        name (str): structure name

    Returns:
        dict[str, Any]: the mesh table
    """
    if not name:
        name = cst.SELECTED_ROR
    dois = cst.ror_affiliations.loc[cst.ror_affiliations.ror == name].doi.tolist()[0]
    meshs = (
        cst.raw_meshs.loc[cst.raw_meshs.doi.isin(dois)]
        .FullMeSH.value_counts()
        .to_frame()
        .reset_index()
        .rename(columns={"index": "MeSH", "FullMeSH": "Compte"})
    )
    return meshs.to_dict("records")


@callback(
    Output("structure-table-Keywords", "data"),
    Input("structure-select-dropdown", "value"),
)
def update_keyword_table(name) -> dict[str, Any]:
    """updates keyword table of selected structure

    Args:
        name (str): structure name

    Returns:
        dict[str, Any]: the keyword table
    """
    if not name:
        name = cst.SELECTED_ROR
    dois = cst.ror_affiliations.loc[cst.ror_affiliations.ror == name].doi.tolist()[0]
    key = (
        cst.raw_keywords.loc[cst.raw_keywords.doi.isin(dois)]
        .NormalizedKeywords.value_counts()
        .to_frame()
        .reset_index()
        .rename(columns={"index": "Mots-Clés", "NormalizedKeywords": "Compte"})
    )
    return key.to_dict("records")


@callback(
    Output("download-institutions-json", "data"),
    Input("menu-download-institutions-json", "n_clicks"),
    State("structure-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_institutions_json(n_clicks: int, indices: list[int]):
    """dowloads ror_affiliations.json

    Args:
        n_clicks (int): unused
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    name = r"backend ror_affiliations.json"
    data = cst.resource_data[name].iloc[indices, :].to_json
    return dcc.send_data_frame(data, "ror_affiliations.json", indent=2)


@callback(
    Output("download-institutions-csv", "data"),
    Input("menu-download-institutions-csv", "n_clicks"),
    State("structure-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_institutions_csv(n_clicks: int, indices: list[int]):
    """dowloads ror_affiliations.csv

    Args:
        n_clicks (int): unused
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    name = r"backend ror_affiliations.json"
    data = cst.resource_data[name].iloc[indices, :].to_csv
    return dcc.send_data_frame(data, "ror_affiliations.csv", sep=";")


@callback(
    Output("download-institutions-xlsx", "data"),
    Input("menu-download-institutions-xlsx", "n_clicks"),
    State("structure-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_institutions_xlsx(n_clicks: int, indices: list[int]):
    """dowloads ror_affiliations.xlsx

    Args:
        n_clicks (int): unused
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    name = r"backend ror_affiliations.json"
    data = cst.resource_data[name].iloc[indices, :].to_excel
    return dcc.send_data_frame(data, "ror_affiliations.xlsx", index=False)


@callback(
    Output("structure-graph-card-hearder", "children"),
    Input("structure-select-dropdown", "value"),
)
def update_graph_header_structure(name) -> list[Any]:
    """callback updates title of structure link graph title

    Args:
        name (str): structure name

    Returns:
        str: title
    """
    if not name:
        name = cst.SELECTED_ROR
    return [
        f"Graphe des liens de co-autorat de {name} ",
        cmp.info_icon("structure_graph_info_badge_id"),
        cmp.popover("structure_graph_info_badge_id", mds.MDS["graphs.md"]),
    ]


@callback(
    Output("structure-graph", "elements"),
    Input("structure-select-dropdown", "value"),
    Input("structure-depth-limit", "value"),
    prevent_initial_call=True,
)
def update_graph_content_structures(name, depth: int) -> Graph:
    """callback updates link graph content

    Returns:
        Graph: the graph item
    """
    if not name:
        name = cst.SELECTED_ROR
    (assembled_graph, _, _) = network.sub_graph(cst.graph_data["structures"].full_graph, name, depth_limit=depth)
    return assembled_graph


@callback(
    Output("structure-graph", "stylesheet"),
    Input("structure-select-dropdown", "value"),
    Input("structure-graph", "tapNode"),
    Input("structure-graph-reset", "n_clicks_timestamp"),
    prevent_initial_call=True,
)
def structures_display_tap_node_data(_, node, n_clicks_timestamp):
    """callback displays the information of the selected node

    Args:
        node (dict): TapNode dict element info

    Returns:
        str: a string containing info on the tapped node
    """
    if not n_clicks_timestamp:
        n_clicks_timestamp = 0
    if node:
        timestamps = {
            "table": cst.STRUCTURE_TABLE_CLICK_TIMESTAMP,
            "node": node["timeStamp"],
            "reset": n_clicks_timestamp,
        }
        choice = max(timestamps, key=timestamps.get)
        if choice in ["table", "reset"]:
            return cst.cyto_stylesheet
        else:
            stylesheet = [
                {
                    "selector": "node",
                    "style": {
                        "opacity": 0.3,
                        "label": "data(label)",
                        "text-opacity": 0.3,
                    },
                },
                {
                    "selector": "edge",
                    "style": {
                        "opacity": 0.2,
                        "curve-style": "bezier",
                    },
                },
                {
                    "selector": f'node[id = "{node["data"]["id"]}"]',
                    "style": {
                        "background-color": "#036287",
                        "border-color": "#2570f1",
                        "border-width": 1,
                        "border-opacity": 1,
                        "opacity": 1,
                        "label": "data(label)",
                        "color": "#036287",
                        "text-opacity": 1,
                        "font-size": 20,
                        "z-index": 9999,
                    },
                },
            ]

            for edge in node["edgesData"]:
                if edge["source"] == node["data"]["id"]:
                    stylesheet.append(
                        {
                            "selector": f'node[id = "{edge["target"]}"]',
                            "style": {
                                "background-color": "#a272ff",
                                "opacity": 0.9,
                                "content": "data(label)",
                                "text-opacity": 1,
                                "color": "#a272ff",
                            },
                        }
                    )
                    stylesheet.append(
                        {
                            "selector": f'edge[id= "{edge["id"]}"]',
                            "style": {
                                "mid-target-arrow-color": "#a272ff",
                                "mid-target-arrow-shape": "vee",
                                "line-color": "#a272ff",
                                "opacity": 0.9,
                                "z-index": 5000,
                            },
                        }
                    )

                if edge["target"] == node["data"]["id"]:
                    stylesheet.append(
                        {
                            "selector": f'node[id = "{edge["source"]}"]',
                            "style": {
                                "background-color": "#06303a",
                                "opacity": 0.9,
                                "z-index": 9999,
                                "content": "data(label)",
                                "text-opacity": 1,
                                "color": "#06303a",
                            },
                        }
                    )
                    stylesheet.append(
                        {
                            "selector": f'edge[id= "{edge["id"]}"]',
                            "style": {
                                "mid-target-arrow-color": "#06303a",
                                "mid-target-arrow-shape": "vee",
                                "line-color": "#06303a",
                                "opacity": 1,
                                "z-index": 5000,
                            },
                        }
                    )
            return stylesheet
    else:
        return cst.cyto_stylesheet


@callback(
    Output("structure-graph", "generateImage"),
    Input("structures-download-graph-button", "n_clicks"),
    State("structure-graph", "elements"),
    prevent_initial_call=True,
)
def unique_structures_get_image(_, elements: list) -> dict:
    """callback gets the image and preps for download

    Args:
        elements (list): list of the elements from cytograph to download

    Returns:
        dict: the download info
    """
    filename = f"Cyto_graph_source_{elements[0]['data']['label']}"
    return {
        "type": "svg",
        "action": "download",
        "filename": filename,
    }


@callback(
    Output("structure-graph-tapnode-out", "data"),
    Input("structure-graph", "tapNode"),
    prevent_initial_call=True,
)
def update_tapnode_data(node) -> str:
    """updates main card title

    Returns:
        str: the new structure name
    """
    return cst.front_ror_affiliations.loc[cst.front_ror_affiliations["Structure"].str.contains(node["data"]["id"])].to_dict("records")  # type: ignore
