import html
from typing import Any, Union

import dash_bootstrap_components as dbc
import dash_cytoscape as cyto
from dash import dash_table, dcc, html

import app.constants as cst
import lib.markdown_elements as mde


def info_icon(id_: str) -> html.I:  # type: ignore
    """an info icon

    Args:
        id_ (str): the id of the dbc.Badge, must be unqiue

    Returns:
        html.I: an icon
    """
    return html.I(
        id=id_,
        style={"color": "var(--purple)"},
        className="bi bi-info-circle",
    )


updated_when_badge: html.H4 = html.H4(
    dbc.Badge(
        f"Données M.A.J. le {cst.stamp.day}/{cst.stamp.month}/{cst.stamp.year}",
        color="secondary",
        style={
            "position": "absolute",
            "top": "10px",
            "right": "10px",
        },
        pill=True,
    ),
)


def popover(target: str, text: Union[str, list[str]]) -> dbc.Popover:
    """a dbc.Popover element

    Args:
        target (str): the target of which the popover is displayed
        text (Union[str, list[str]]): a string or list of strings in Markdown form

    Returns:
        dbc.Popover: the generated dbc.Popover element
    """
    return dbc.Popover(
        dcc.Markdown(text),
        target=target,
        body=True,
        trigger="hover",
    )


def dropdown_table_download(label, id_: str, color="light") -> dbc.DropdownMenu:
    """a dropdown menu with .json, .csv and .xlsx formats

    Args:
        id (str): a string to make ids of components unique

    Returns:
        dbc.DropdownMenu: the dropdown menu
    """
    return dbc.DropdownMenu(
        label=f"{label} ",
        color=color,
        children=[
            dbc.DropdownMenuItem(
                [
                    "Format json ",
                    html.I(className="bi bi-filetype-json"),
                    dcc.Download(id=f"download-{id_}-json"),
                ],
                id=f"menu-download-{id_}-json",
            ),
            dbc.DropdownMenuItem(
                [
                    "Format csv ",
                    html.I(className="bi bi-filetype-csv"),
                    dcc.Download(id=f"download-{id_}-csv"),
                ],
                id=f"menu-download-{id_}-csv",
            ),
            dbc.DropdownMenuItem(
                [
                    "Format xlsx ",
                    html.I(className="bi bi-filetype-xlsx"),
                    dcc.Download(id=f"download-{id_}-xlsx"),
                ],
                id=f"menu-download-{id_}-xlsx",
            ),
        ],
    )


def generate_dashtable_element(id_: str, columns: list, data: list, **kwargs) -> dash_table.DataTable:
    """generate dashtable dash element

    Args:
        id_(str):
        columns (list):
        data (list):
        **kwargs:
            style_table (dict, optional): Defaults to style_table
            style_cell (dict, optional): Defaults to style_cell
            style_data_conditional (list, optional): Defaults to style_data_conditional
            fixed_columns (dict, optional): Defaults to {"headers": False, "data": 0}
            page_size (int, optional): Defaults to 10
            filter_action (str , optional): Defaults to "native"
            sort_action (str, optional): Defaults to "native"

    Returns:
        dash_table.DataTable: the initialized DashTable element
    """
    supported_arguments = {
        "style_table": {"height": "600px", "maxWidth": "100%", "overflowY": "scroll", "font-family": "'Montserrat', sans-serif;"},
        "style_cell": {
            "height": "auto",
            "minWidth": "50px",
            "width": "180px",
            "maxWidth": "300px",
            "textAlign": "center",
            "fontSize": 15,
            "font-family": "'Montserrat', sans-serif",
        },
        "style_data_conditional": [
            {
                "if": {"state": "selected"},
                "backgroundColor": "#5DEEE3",
                "border": "1px solid #5DEEE3",
            },
            {
                "if": {"row_index": "odd"},
                "backgroundColor": "#F2F7F9",
                "border": "#F2F7F9",
            },
        ],
        "fixed_columns": {"headers": False, "data": 0},
        "fixed_rows": {"headers": True},
        "page_size": 30,
        "filter_action": "native",
        "sort_action": "native",
        "style_as_list_view": True,
    }
    for arg, value in kwargs.items():
        if arg in supported_arguments:
            supported_arguments[arg] = value
    return dash_table.DataTable(
        id=id_,
        columns=columns,
        data=data,
        fixed_columns=supported_arguments["fixed_columns"],
        fixed_rows=supported_arguments["fixed_rows"],
        style_table=supported_arguments["style_table"],
        style_data={
            "whiteSpace": "normal",
            "vertical-align": "top",
        },
        style_header={
            "fontWeight": "bold",
            "backgroundColor": "white",
            "height": "auto",
            "minWidth": "100px",
            "width": "110px",
            "maxWidth": "180px",
            "whiteSpace": "normal",
            "fontSize": 18,
        },
        style_data_conditional=supported_arguments["style_data_conditional"],
        page_size=supported_arguments["page_size"],
        filter_action=supported_arguments["filter_action"],
        filter_options={"case": "insensitive"},
        sort_action=supported_arguments["sort_action"],
        style_cell=supported_arguments["style_cell"],
        style_as_list_view=supported_arguments["style_as_list_view"],
    )


end_page_accordion: dbc.Accordion = dbc.Accordion(
    [
        dbc.AccordionItem(
            dcc.Markdown(mde.MDS["contributors_and_mainenance.md"]),
            title="Contributeurs et maintenance",
        ),
        dbc.AccordionItem(
            dcc.Markdown(mde.MDS["mentions_legales.md"]),
            title="Mentions légales",
        ),
        dbc.AccordionItem(
            dcc.Markdown(mde.MDS["politique_confidentialitee.md"]),
            title="Politique de confidentialité",
        ),
        dbc.AccordionItem(
            dcc.Markdown(mde.MDS["gestions_cookies.md"]),
            title="Politique de gestion des Cookies et autres traceurs",
        ),
    ],
    start_collapsed=True,
)


def download_button(id_: str) -> dbc.Button:
    """download button

    Returns:
        dbc.Button: the button
    """
    return dbc.Button(
        ["Exporter ", html.I(className="bi bi-filetype-svg")],
        id=id_,
        color="secondary",
        size="sm",
        style={"float": "right"},
    )


def generate_cytograph(id_: str, layout: dict, style: dict, elements, stylesheet: list) -> cyto.Cytoscape:
    """generate Cytoscape Graph dash element

    Args:
        id_ (str): the unique id of the element
        layout (dict): the layout dict of the cyto.Cytoscape element
        style (dict): the style dict of the cyto.Cytoscape element
        elements ([type]): [description]
        stylesheet (list): the stylesheet list of the cyto.Cytoscape element

    Returns:
        cyto.Cytoscape: cyto.Cytoscape element
    """
    return cyto.Cytoscape(
        id=id_,
        layout=layout,
        style=style,
        elements=elements,
        stylesheet=stylesheet,
        maxZoom=2,
        minZoom=0.3,
    )


# half graphs elements
def col_generate_half_plot(title: list[Any], id_: str) -> dbc.Col:
    """generates a half plot element

    Args:
        title (list[Any]): the title of the element
        id_ (str): the unique id of the element

    Returns:
        dbc.Col: the Col with a Card containing the graph element
    """
    return dbc.Col(
        [
            dbc.Card(
                [
                    dbc.CardHeader([html.H5(title)]),
                    dbc.CardBody(
                        dbc.Spinner(
                            dcc.Graph(id=id_),
                            color="primary",
                            spinner_style={"width": "3rem", "height": "3rem"},
                        )
                    ),
                ],
            ),
        ],
    )


# Full graph elements
def card_generate_full_plots(title: list[Any], id_: str) -> dbc.Card:
    """generates a full plot element

    Args:
        title (list[Any]): the title of the element
        id_ (str): the unique id of the element

    Returns:
        dbc.Card: the Card containing a graph element
    """
    return dbc.Card(
        [
            dbc.CardHeader([html.H5(title)]),
            dbc.CardBody(
                dbc.Spinner(
                    dcc.Graph(id=id_),
                    color="primary",
                    spinner_style={"width": "3rem", "height": "3rem"},
                )
            ),
        ],
        className="dbc",
    )


logo: html.A = html.A(
    html.Img(
        src=f"data:image/png;base64,{cst.png_files['Health-Data-Hub-logo_bleu_HD.png']}",
        style={"width": "100%"},
    ),
    href="https://www.health-data-hub.fr/",
    className="logo",
)

# Intro
card_nbpubli: dbc.Card = dbc.Card(
    [
        html.H4(str(cst.publications.shape[0]), style=cst.style_numbers),
        html.H6("Publications sur le SNDS dans Pubmed, LiSSa & HAL", style=cst.style_text),
    ],
    id="nbpublications",
    style=cst.style_cards[4],
    body=True,
)
card_nbauth: dbc.Card = dbc.Card(
    [
        html.H4(str(cst.authors.shape[0]), style=cst.style_numbers),
        html.H6("Auteurs différents", style=cst.style_text),
    ],
    id="nbauteurs",
    style=cst.style_cards[0],
    body=True,
)
card_nb_inst: dbc.Card = dbc.Card(
    [
        html.H4(f"{str(cst.ror_affiliations.shape[0])}*", style=cst.style_numbers),
        html.H6("Structures différentes", style=cst.style_text),
        html.Small(
            "*Nombre approximatif, calculé grace aux affiliations des auteurs",
            style=cst.style_small_text,
        ),
    ],
    id="nbinst",
    style=cst.style_cards[1],
    body=True,
)
card_nbcountries: dbc.Card = dbc.Card(
    [
        html.H4(
            f"{str(len(set(cst.ror_affiliations.country.apply(lambda x: x['country_name']))))}*",
            style=cst.style_numbers,
        ),
        html.H6("Pays", style=cst.style_text),
        html.Small(
            "*Nombre approximatif, calculé grace aux affiliations des auteurs",
            style=cst.style_small_text,
        ),
    ],
    id="nbcountries",
    style=cst.style_cards[2],
    body=True,
)
card_meancit: dbc.Card = dbc.Card(
    [
        html.H4(str(round(cst.publications.PmcRefCount.mean(), 2)), style=cst.style_numbers),
        html.H6("Citations moyenne par article", style=cst.style_text),
    ],
    id="citations",
    style=cst.style_cards[3],
    body=True,
)

# Meshs
col_uniformmesh_cloud: dbc.Col = dbc.Col(
    [
        dbc.CardImg(
            src=f"data:image/png;base64,{cst.png_files['allMeSH_wordcloud.png']}",
            top=True,
        ),
        dbc.CardFooter("Nuage de MeSH"),
    ]
)
col_uniformmesh_table: dbc.Col = dbc.Col(
    [
        generate_dashtable_element(
            id_="MeSH_freqs_table",
            columns=[{"name": i, "id": i} for i in cst.resource_data[r"uniform_mesh_freqs.json"].columns],
            data=cst.resource_data[r"uniform_mesh_freqs.json"].sort_values(by=["Count"], ascending=False).to_dict("records"),
            style_table={
                "height": "600px",
                "maxWidth": "100%",
            },
            style_cell={
                "height": "auto",
                "whiteSpace": "normal",
                "font-family": "'Montserrat', sans-serif",
            },
            fixed_rows={"headers": False},
            page_size=17,
        ),
    ],
)
col_mainmesh_cloud: dbc.Col = dbc.Col(
    [
        dbc.CardImg(
            src=f"data:image/png;base64,{cst.png_files['mainMeSH_wordcloud.png']}",
            top=True,
        ),
        dbc.CardFooter("Nuage de Main MeSH (*)"),
    ]
)
col_mainmesh_table: dbc.Col = dbc.Col(
    [
        generate_dashtable_element(
            id_="mainMeSH_freqs_table",
            columns=[{"name": i, "id": i} for i in cst.resource_data[r"main_mesh_freqs.json"].columns],
            data=cst.resource_data[r"main_mesh_freqs.json"].sort_values(by=["Count"], ascending=False).to_dict("records"),
            style_table={
                "height": "600px",
                "maxWidth": "100%",
            },
            style_cell={
                "height": "auto",
                "whiteSpace": "normal",
                "font-family": "'Montserrat', sans-serif",
            },
            fixed_rows={"headers": False},
            page_size=17,
        ),
    ],
)

# Keywords
col_keyword_cloud: dbc.Col = dbc.Col(
    [
        dbc.CardImg(
            src=f"data:image/png;base64,{cst.png_files['keyword_wordcloud.png']}",
            top=True,
        ),
        dbc.CardFooter("Nuage de Mots-clés"),
    ]
)
col_keyword_table: dbc.Col = dbc.Col(
    [
        generate_dashtable_element(
            id_="keywords_freqs_table",
            columns=[{"name": i, "id": i} for i in cst.resource_data[r"keyword_freqs.json"].columns],
            data=cst.resource_data[r"keyword_freqs.json"].sort_values(by=["Count"], ascending=False).to_dict("records"),
            style_table={
                "height": "600px",
                "maxWidth": "100%",
            },
            style_cell={
                "height": "auto",
                "whiteSpace": "normal",
                "font-family": "'Montserrat', sans-serif",
            },
            fixed_rows={"headers": False},
            page_size=17,
        ),
    ],
)
