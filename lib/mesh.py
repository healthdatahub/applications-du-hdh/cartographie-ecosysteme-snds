from typing import Any, Optional, Union

import pandas as pd


def get_mesh_key(element: dict[str, Any], key: str) -> Optional[str]:
    """gets mesh info from MeshHeadingList column

    Args:
        element (dict[str, Any]): element from pandas
        key (str): key in dict

    Returns:
        Optional[Any]: None or str
    """
    try:
        if isinstance(element["QualifierName"], dict):
            return element["QualifierName"][key]
        items = []
        for item in element["QualifierName"]:
            items.append(item[key])
        return " / ".join(items)
    except KeyError:
        return None


def mesh_stats(res: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """returns mesh stats

    Args:
        res (Union[pd.DataFrame, pd.Series]): the all_DB

    Returns:
        Union[pd.DataFrame, pd.Series]: mesh stats dataframe
    """
    res = res.dropna(subset=["MeshHeadingList"]).explode("MeshHeadingList")  # type: ignore
    res.index.names = ["doi"]
    res = res.MeshHeadingList.reset_index()
    mesh = pd.DataFrame()
    mesh["doi"] = res["doi"]
    mesh["DescriptorName"] = res["MeshHeadingList"].apply(lambda x: x["DescriptorName"]["#text"])
    mesh["DescriptorMajorTopic"] = res["MeshHeadingList"].apply(lambda x: x["DescriptorName"]["@MajorTopicYN"])
    mesh["QualifierName"] = res["MeshHeadingList"].apply(lambda x: get_mesh_key(x, "#text"))
    mesh["QualifierMajorTopic"] = res["MeshHeadingList"].apply(lambda x: get_mesh_key(x, "@MajorTopicYN"))
    mesh["FullMeSH"] = mesh[["DescriptorName", "QualifierName"]].values.tolist()
    mesh["FullMeSH"] = mesh["FullMeSH"].apply(lambda x: " / ".join(filter(None, x)))
    return mesh


def mesh_full_stats(mesh: pd.DataFrame) -> Union[pd.DataFrame, pd.Series]:
    """does stats for all meshes

    Args:
        mesh (pd.DataFrame): output from mesh_stats(*args)

    Returns:
        Union[pd.DataFrame, pd.Series]: all mesh stats dataframe
    """
    mesh = mesh.groupby("FullMeSH").size().reset_index(name="Count").sort_values(by=["Count"], ascending=False)
    return mesh


def main_mesh_stats(mesh: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """does stats for main meshes

    Args:
        mesh (Union[pd.DataFrame, pd.Series]): output from mesh_stats(*args)

    Returns:
        Union[pd.DataFrame, pd.Series]: main mesh stats dataframe
    """
    mesh = mesh.loc[mesh.DescriptorMajorTopic == "Y"].groupby("FullMeSH").size().reset_index(name="Count").sort_values(by=["Count"], ascending=False)
    mesh = mesh.rename(columns={"FullMeSH": "MainMeSH"})
    return mesh
