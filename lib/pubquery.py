QUERY = """
(
    "medico administrative data*"[Title/Abstract]
    OR "medicoadministrative data*"[Title/Abstract]
    OR "national health data system"[Title/Abstract]
    OR "french national health database*"[Title/Abstract]
    OR "snds"[Title/Abstract]
    OR "pmsi"[Title/Abstract]
    OR "Medical Information Systems Program"[Title/Abstract]
    OR "french hospital discharge database"[Title/Abstract:~1]
    OR "french hospital discharge databases"[Title/Abstract:~1]
    OR "sniiram"[Title/Abstract]
    OR "sniir am"[Title/Abstract]
    OR "national health insurance information system"[Title/Abstract]
    OR "egb database"[Title/Abstract]
    OR "egb sample"[Title/Abstract:~10]
    OR "echantillon generaliste des beneficiaires"[Title/Abstract]
    OR "french national healthcare insurance"[Title/Abstract:~1]
    OR "french national healthcare database"[Title/Abstract:~1]
    OR "french national healthcare databases"[Title/Abstract:~1]
    OR "french national health data"[Title/Abstract:~1]
    OR "french national health database"[Title/Abstract:~1]
    OR "french national hospital database"[Title/Abstract:~1]
    OR "french national hospital discharge"[Title/Abstract:~1]
    OR "french health insurance database"[Title/Abstract:~1]
    OR "systeme national des donnees de sante"[Title/Abstract]
    OR "national health insurance system database"[Title/Abstract:~1]
    OR "french health insurance system database"[Title/Abstract]
    OR "national administrative database"[Title/Abstract:~1]
    OR "french medical administrative database"[Title/Abstract]
    OR "information systems medicalization program"[Title/Abstract]
)
AND
(
    "france"[Title/Abstract]
    OR "french"[Title/Abstract]
)
AND
(
    "2007/01/01"[Date - Publication] : "3000"[Date - Publication]
)
"""


QUERY_BIS = """
(
    "medical administrative data*"[Title/Abstract]
    OR "health administrative database"[Title/Abstract]
    OR "french claims data"[Title/Abstract]
    OR "national health insurance database"[Title/Abstract] 
    OR "french national claims data"[Title/Abstract]
    OR "administrative claims data"[Title/Abstract]
    OR "french health care databases"[Title/Abstract]
    OR "french nationwide health database"[Title/Abstract:~1]
    OR "nationwide hospital database"[Title/Abstract:~1]
    OR "French administrative hospital discharge database"[Title/Abstract]
    OR "national hospital discharge database"[Title/Abstract]
    OR "administrative hospital-discharge database"[Title/Abstract]
)
AND
(
    "france"[Title/Abstract]
    OR "french"[Title/Abstract]
)
AND
(
    "2007/01/01"[Date - Publication] : "3000"[Date - Publication]
)
"""


QUERY_PMSI = """
(
    "pmsi"[Title/Abstract]

)
NOT
(
    "france"[Title/Abstract]
    OR "french"[Title/Abstract]
    OR "post-mortem submersion interval"
    OR "postmortem submersion interval"
)

AND
(
    "france"[Supplemantary Concept]
    OR "french"[Supplemantary Concept]
)

AND
(
    "2007/01/01"[Date - Publication] : "3000"[Date - Publication]
)
"""
