"""script reads the markdowns"""
from os import walk

MDS: dict[str, list[str]] = {}

dir_, _, filenames = next(walk("assets/mds/"), (None, None, []))
for file_ in filenames:
    with open(rf"{dir_}{file_}", "r", encoding="utf-8") as _:
        MDS[file_] = _.read().splitlines()
