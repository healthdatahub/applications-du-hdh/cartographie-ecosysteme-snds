import glob
import json
import logging
import os
from dataclasses import dataclass
from typing import Optional, Union

import networkx as nx
import pandas as pd

with open("app.log", "w") as file_:
    file_.write("")
logging.basicConfig(
    encoding="utf-8",
    level=logging.DEBUG,
    format="%(asctime)s %(message)s",
    handlers=[logging.FileHandler("app.log"), logging.StreamHandler()],
)


def reformat_pmids_in_links(element: str) -> str:
    """makes PMIDs into site with direct link to PubMed

    Args:
        element (int): a DataFrame Cell

    Returns:
        str: the formatted DataFrame Cell into markdown link
    """
    if isinstance(element, str):
        element = f"[{element}](https://pubmed.ncbi.nlm.nih.gov/{element}/)"
    return element


def reformat_authors_in_links(element: str) -> str:
    """makes authors into site with direct link to PubMed

    Args:
        element (int): a DataFrame Cell

    Returns:
        str: the formatted DataFrame Cell into markdown link
    """
    if isinstance(element, str):
        element = f"[{element}](https://pubmed.ncbi.nlm.nih.gov/?sort=pubdate&show_snippets=off&term={element.replace(' ', '+')})"
    return element


def reformat_ror_in_links(element: str, data: Union[pd.DataFrame, pd.Series]) -> str:
    """makes rors into links

    Args:
        element (str): a DataFrame Cell
        data (Union[pd.DataFrame, pd.Series]): the DataFrame

    Returns:
        str: the formatted DataFrame Cell into markdown link
    """
    return f"[{element}]({data.id[data.loc[data.Structure == element].index].item()})"


def reformat_strings_list_md(element: str, pmid: bool = False) -> str:
    """reformats a string seperated by a ';' for markdown with list

    Args:
        element (str): a DataFrame Cell

    Returns:
        str: the formatted DataFrame Cell
    """
    noinfo = "N/A"
    if isinstance(element, list):
        if pmid:
            element = ";".join(map(lambda x: reformat_pmids_in_links(str(x)), element))
        else:
            element = ";".join(map(str, element))
        if ";" in element:
            listreformatted = element.split(";")
            nreformatted = []
            for elem in listreformatted:
                if not elem:
                    elem = noinfo
                # MeSH
                if "*" in elem:
                    nreformatted.append(f"- **{elem.strip()}*\n")
                else:
                    nreformatted.append(f"- {elem.strip()}\n")
            reformatted = "".join(nreformatted)
        else:
            reformatted = f"- {element}"
            if reformatted == "- ":
                reformatted = f"- {noinfo}"
        return reformatted.replace("None", noinfo).replace("null", noinfo)
    else:
        return element


def reformat_strings_links_md(element: str) -> str:
    """makes chosen site cell into markdown link

    Args:
        element (str): a DataFrame Cell

    Returns:
        str: the formatted DataFrame Cell into markdown link
    """
    if isinstance(element, str):
        element = f"<https://{element}>"
    return element


@dataclass
class ReadDataFrame:
    """class for reading data from resources for a dashboard element"""

    path_backend_dataframe: str
    frontend_is_backend: bool = True
    backend_dataframe: Union[pd.DataFrame, pd.Series] = pd.DataFrame()
    frontend_dataframe: Union[pd.DataFrame, pd.Series] = pd.DataFrame()

    def read_dataframe(self):
        """Reads a DataFrame in json format and stores it."""
        try:
            self.backend_dataframe = pd.read_json(self.path_backend_dataframe)
            logging.info(f"Reading {self.path_backend_dataframe}")
            if not self.frontend_is_backend:
                logging.info("reformatting for frontend")
                self.frontend_dataframe = self.backend_dataframe
                self.frontend_dataframe = self.reformat_for_frontend()
        except ValueError as err:
            logging.info(f"Unexpected error while reading data {self.path_backend_dataframe}:\n{err}")

    def reformat_for_frontend(self) -> Union[pd.DataFrame, pd.Series]:
        """reformats backend DataFrames in frontend, i.e. renaming columns and markdown formatting

        Returns:
            Union[pd.DataFrame, pd.Series]: the frontend DataFrame
        """
        if self.path_backend_dataframe == os.path.join("data/resources", "authors.json"):
            renaming = {
                "FullName": "Nom(s)",
                "AffiliationInfo": "Affiliations",
            }
            self.frontend_dataframe = self.frontend_dataframe.rename(columns=renaming)
            for col in self.frontend_dataframe.columns:
                if col in ["Affiliations", "doi"]:
                    self.frontend_dataframe[col] = self.frontend_dataframe[col].apply(reformat_strings_list_md)
            self.frontend_dataframe["PMID"] = self.frontend_dataframe["PMID"].apply(reformat_strings_list_md, pmid=True)
            self.frontend_dataframe["Nom(s)"] = self.frontend_dataframe["Nom(s)"].apply(reformat_authors_in_links)
        if self.path_backend_dataframe == os.path.join("data/resources", "publications.json"):
            renaming = {
                "ArticleTitle": "Titre",
                "articleDB": "Provenance",
                "FullName": "Auteurs",
                "Journal-Title": "Journal",
                "PubDate": "Date de publication",
                "PmcRefCount": "Nombre de citations",
                "KeywordList": "Mots-clés",
                "MeshHeadingList": "MeSHs",
                "AffiliationInfo": "Affiliations",
                "ArticleType": "Type d'article",
            }
            self.frontend_dataframe = self.frontend_dataframe.rename(columns=renaming)
            for col in ["Auteurs", "Affiliations"]:
                self.frontend_dataframe[col] = self.frontend_dataframe[col].apply(reformat_strings_list_md)
            self.frontend_dataframe = self.frontend_dataframe.sort_values(by="Date de publication", ascending=False)
            self.frontend_dataframe["PMID"] = self.frontend_dataframe["PMID"].astype(str).apply(reformat_pmids_in_links)
        elif self.path_backend_dataframe == os.path.join("data/resources", "ror_affiliations.json"):
            renaming = {
                "ror": "Structure",
                "PMID": "PMID",
                "doi": "doi",
                "FullName": "Auteurs",
                "country": "Pays",
                "addresses": "Ville",
            }
            self.frontend_dataframe = self.frontend_dataframe.rename(columns=renaming)
            self.frontend_dataframe["Structure"] = self.frontend_dataframe["Structure"].apply(
                lambda x: reformat_ror_in_links(x, self.frontend_dataframe)
            )
            self.frontend_dataframe["doi"] = self.frontend_dataframe["doi"].apply(reformat_strings_list_md)
            self.frontend_dataframe["PMID"] = self.frontend_dataframe["PMID"].apply(reformat_strings_list_md, pmid=True)
            self.frontend_dataframe["Auteurs"] = self.frontend_dataframe["Auteurs"].apply(reformat_strings_list_md)
            self.frontend_dataframe["Pays"] = self.frontend_dataframe["Pays"].apply(lambda x: x["country_name"])
            self.frontend_dataframe["Ville"] = self.frontend_dataframe["Ville"].apply(lambda x: x[0]["city"] if isinstance(x, list) else None)
        return self.frontend_dataframe


def read_data() -> dict[str, Union[pd.DataFrame, pd.Series]]:
    """reads the local jsons and stores the data in pd.DataFrame form in a dict

    Returns:
        dict[str, Union[pd.DataFrame, pd.Series]]: the dict with path of file as key and it's data in pd.DataFrame
    """
    data_files = glob.glob("data/resources/*.json")
    readdata: dict[str, Union[pd.DataFrame, pd.Series]] = {}
    for file_ in data_files:
        if file_ in [
            os.path.join("data/resources", "authors.json"),
            os.path.join("data/resources", "publications.json"),
            os.path.join("data/resources", "ror_affiliations.json"),
        ]:
            readdataclass = ReadDataFrame(file_, frontend_is_backend=False)
            readdataclass.read_dataframe()
            readdata[os.path.basename(file_)] = readdataclass.frontend_dataframe
            readdata[f"backend {os.path.basename(file_)}"] = readdataclass.backend_dataframe
        else:
            readdataclass = ReadDataFrame(file_)
            readdataclass.read_dataframe()
            readdata[os.path.basename(file_)] = readdataclass.backend_dataframe
    return readdata


@dataclass
class ReadDataGraph:
    """class for reading data from resources for a dashboard element"""

    path_graph: str
    default_node: str = ""
    default_depth_limit: int = 2
    full_graph: nx.classes.graph.Graph = nx.Graph()
    sub_graph: Optional[list[dict]] = None

    def read_full_and_sub_graph(self):
        """Reads full and sub graphs from and stores it."""
        full_graph_path = glob.glob(f"{self.path_graph}/*.gml")[0]
        sub_graph_path = glob.glob(f"{self.path_graph}/*.json")[0]
        try:
            self.full_graph = nx.read_gml(full_graph_path)
            logging.info(f"Reading {full_graph_path}")
        except ValueError as err:
            logging.info(f"Unexpected error while reading data {self.full_graph}:\n{err}")
        try:
            with open(sub_graph_path, "r", encoding="utf-8") as file_:
                self.sub_graph = json.load(file_)
                logging.info(f"Reading {sub_graph_path}")
                split_name = os.path.basename(sub_graph_path).split("_")
                self.default_node = split_name[0]
                self.default_depth_limit = int(split_name[1])
        except ValueError as err:
            logging.info(f"Unexpected error while reading data {self.sub_graph}:\n{err}")


def read_graphs() -> dict[str, ReadDataGraph]:
    """reads the local graph files and stores them in ReadDataGraph class

    Returns:
        dict[str, ReadDataGraph]: a dict with key the folder and value the ReadDataGraph
    """
    graphs = {}
    _, dirs, _ = next(os.walk("data/resources/graphs/"))
    for dir_ in dirs:
        graphinfo = ReadDataGraph(os.path.join("data/resources/graphs/", dir_))
        graphinfo.read_full_and_sub_graph()
        graphs[dir_] = graphinfo
    return graphs
