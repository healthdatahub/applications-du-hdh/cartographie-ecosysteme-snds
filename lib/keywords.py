from typing import Union

import pandas as pd
import unidecode


def keyword_stats(res: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """returns keywords stats

    Args:
        res (Union[pd.DataFrame, pd.Series]): the all_DB

    Returns:
        Union[pd.DataFrame, pd.Series]: keywords stats dataframe
    """
    keywords = res.dropna(subset="KeywordList").explode("KeywordList")  # type: ignore
    keywords.index.names = ["doi"]
    keywords = keywords.KeywordList.reset_index()
    keywords["RawKeywords"] = keywords.KeywordList.apply(lambda x: x["#text"] if isinstance(x, dict) else None)
    keywords["NormalizedKeywords"] = keywords["RawKeywords"].dropna().apply(str.lower).apply(unidecode.unidecode)
    return keywords


def keyword_freqs(keywords: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """returns keywords freqs

    Args:
        res (Union[pd.DataFrame, pd.Series]): the raw keyword stats

    Returns:
        Union[pd.DataFrame, pd.Series]: keywords freqs
    """
    keywords = keywords.NormalizedKeywords.value_counts()
    freqs = pd.DataFrame()
    freqs["NormalizedKeywords"] = keywords.keys()
    freqs["Count"] = keywords.values
    return freqs
