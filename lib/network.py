"""module contains functions need to search for and return sublist of edges & nodes"""
import itertools
from typing import Generator, Optional

import networkx as nx
import numpy as np
import pandas as pd


def find_adjacencies(data: pd.DataFrame, node_field: str, edge_field: str) -> dict[str, list]:
    """function create adjancency dict

    Args:
        data (pd.DataFrame): the dataframe which is the basis for that adjacency dict
        node_field (str): the node field
        edge_field (str): the edge field

    Returns:
        dict[str, list]: key (node) / value (all nodes connected to key)
    """
    adjacency_matrix = pd.crosstab(data[edge_field], data[node_field])
    all_edges = {}
    for index, row in adjacency_matrix.iterrows():
        all_edges[index] = dict(row.loc[row > 0])
    adjacency_matrix = {key: [] for key in set(data[node_field])}
    for edge in all_edges.values():
        adj = np.array(list(edge.keys()))
        for index, aff in enumerate(adj):
            adjacency_matrix[aff].append(list(adj[np.arange(len(adj)) != index]))
    for aff in adjacency_matrix:
        adjacency_matrix[aff] = list(itertools.chain.from_iterable(adjacency_matrix[aff]))
    return adjacency_matrix


def process_json_cyto(graph: list[dict]) -> list[dict]:
    """function 'remames' *name* key with *label*

    Args:
        graph (list[dict]): a list of dicts with a *name* key needing renamed

    Returns:
        list[dict]: returns the same list of dicts with the renamed key
    """
    for ind in graph:
        ind = ind["data"]
        ind["label"] = ind.pop("name")
    return graph


def create_full_graph(dataframe: pd.DataFrame, node_field: str, edge_field: str) -> nx.Graph:
    """function creates nx.Graph object from a dataframe

    Args:
        dataframe (pd.DataFrame): the read pickled pandas dataframe
        node_field (str): the column of the dataframe which will be the node field
        edge_field (str): the column of the dataframe which will be the edges
    Returns:
        nx.Graph: the nx.Graph object
    """
    graph_adj = find_adjacencies(dataframe, node_field, edge_field)
    graph = nx.Graph(graph_adj)
    return graph


def add_depth(assembled_graph: list[dict], depth_list: list[int]) -> list[dict]:
    """function adds depth to elements

    Args:
        assembled_graph (list[dict]): nodes + elements dicts
        depth_list (list[int]): list of nodes and their respective depths
    Returns:
       assembled_graph : nodes + elements with new field "classes" for nodes which has depth
    """
    intoword = {0: "zero", 1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine", 10: "ten"}
    for node in assembled_graph[: len(depth_list)]:
        node["classes"] = intoword[depth_list[node["data"]["id"]]]
    return assembled_graph


def sub_graph(
    graph: nx.Graph,
    source: Optional[str] = None,
    depth_limit: int = 3,
    largest_connection: bool = False,
) -> tuple[nx.classes.graph.Graph, list[dict], Generator]:
    """function creates the final HTML network graph from the list of sources

    Args:
        graph (nx.Graph): the nx.Graph object generated from the sources Dataframe
        source (Optional[str], optional): the specific name of a node to generate a smaller subgraph. Defaults to None.
        depth_limit (int, optional): max number of edges between 2 nodes taken. Defaults to 3.
        largest_connection (bool, optional): if True will select only connected sources
    """
    list_nodes = list(graph.nodes())
    graph.remove_node(list_nodes[-1])
    list_nodes_normalized = [i for i in list_nodes if isinstance(i, str)]
    if largest_connection:
        largest_cc = max(nx.connected_components(graph), key=len)
        graph = graph.subgraph(largest_cc)
    if not source:
        tree = graph
    else:
        tree = nx.dfs_tree(graph, source=source, depth_limit=depth_limit)
    depths = nx.shortest_path_length(tree, source)
    elements = nx.cytoscape_data(tree)
    nodes = elements["elements"]["nodes"]
    nodes = process_json_cyto(nodes)
    edges = elements["elements"]["edges"]
    assembled_graph = nodes + edges
    assembled_graph = add_depth(assembled_graph, depths)
    return assembled_graph, list_nodes_normalized, depths
