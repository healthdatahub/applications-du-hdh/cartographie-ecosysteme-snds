import lib.hal
import lib.LiSSa
import lib.PubMed


def main():
    """main func"""
    lib.PubMed.main()
    lib.LiSSa.main()
    lib.hal.main()


if __name__ == "__main__":
    main()
