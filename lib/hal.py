"""code to interact with HAL API"""

from pathlib import Path
from typing import Any

import dateutil.parser as parser
import pandas as pd
import requests

URL: str = "https://api.archives-ouvertes.fr/search/"

QUERY: str = """
(
    "medico administrative data"
    OR "medico administrative database"
    OR "medico administrative databases"
    OR "national health fund data"
    OR "national health fund database"
    OR "national health fund databases"
    OR "claims data"
    OR "claims database"
    OR "claims databases"
    OR "national healthcare system"
    OR "national health data system"
    OR "national health database"
    OR "national health databases"
    OR "national health datasets"
    OR "national health insurance reimbursement system"
    OR "snds"
    OR "pmsi"
    OR "Programme de Medicalisation des Systemes d'Information"
    OR "french hospital discharge database"
    OR "cepidc"
    OR "dcir"
    OR "sniiram"
    OR "sniir am"
    OR "national health insurance information system"
    OR "egb database"
    OR "echantillon generaliste des beneficiaires"
    OR "french national healthcare"
    OR "french national healthcare insurance"
    OR "french national healthcare database"
    OR "french national healthcare databases"
    OR "french national health insurance"
    OR "french national health data"
    OR "french national health data system"
    OR "french national hospital database"
    OR "french national hospital discharge"
    OR "french health insurance database"
    OR "french health insurance scheme"
    OR "french administrative healthcare"
    OR "french death certificates"
    OR "systeme national des donnees de sante"
    OR "national health insurance system database"
    OR "french health insurance system"
    OR "french health insurance service"
    OR "nationwide french"
    OR "nationwide french registry"
    OR "national administrative database"
    OR "french medical-administrative database"
    OR "information systems medicalization program"
)
"""

PARAMETERS: dict[str, Any] = {
    "q": QUERY,
    "fq": '("france" OR "french")&&(submittedDateY_i:[2007 TO 2030])',
    "fl": "*_s,docid",
    "wt": "json",
    "rows": 10000,
}


def _split_list(a_list: list[Any]) -> list[dict[str, str]]:
    """splits list into 2 halfs and transforms into dict

    Args:
        a_list (list[Any]): list of LastName and Initials

    Returns:
        list[dict[str, str]]: list of auhtors [{"LastName": "XXXX", "Initials": "X"}]
    """
    half = len(a_list) // 2
    temp = []
    for index, lastname in enumerate(a_list[:half]):
        temp.append({"LastName": lastname, "Initials": a_list[half:][index]})
    return temp


def get_hal_request(url: str, parameters: dict[str, Any]) -> Any:
    """get request the articles from HAL db that meet query parameters

    Args:
        url (str): need url for request
        parameters (dict[str, Any]): need paramters dict

    Returns:
        Any: json
    """
    res = requests.get(url, params=parameters)
    return res.json()


def reformat_respone(res: Any, alldb: Any, doctype: pd.DataFrame) -> pd.DataFrame:
    """reformats HAL request response to be merged with main DB

    Args:
        res (Any): json object result from get_hal_request(*args)
        alldb (Any): read "data/resources/all_DB.json"
        doctype (pd.DataFrame): the doctype.json file

    Returns:
        pd.DataFrame: reformated HAL pandas
    """
    haldb = pd.DataFrame(res["response"]["docs"])
    haldois = haldb.doiId_s.dropna()
    intersection = list(set(alldb.T.index) & set(haldois))
    haldb.loc[haldb["doiId_s"].isnull(), "doiId_s"] = haldb.docid
    haldois = haldb.doiId_s
    haldb = haldb.rename(
        columns={
            "title_s": "ArticleTitle",
            "journalTitle_s": "Journal-Title",
            "journalTitleAbbr_s": "Journal-ISOAbbreviation",
            "abstract_s": "Abstract",
            "language_s": "Language",
            "mesh_s": "MeshHeadingList",
            "keyword_s": "KeywordList",
            "publicationDate_s": "PubDate",
            "authLastName_s": "LastName",
            "docType_s": "ArticleType",
        }
    )
    haldb["PMID"] = None
    haldb["VernacularTitle"] = None
    haldb["PmcRefCount"] = None
    haldb["PubYear"] = None
    haldb["PubYear"] = haldb.PubDate.apply(lambda x: parser.parse(x).year)
    haldb["SortPubDate"] = None
    haldb["articleDB"] = "HAL"
    haldb["Initials"] = haldb.authFirstName_s.apply(lambda x: [name[0] for name in x])
    haldb["AuthorList"] = haldb.LastName + haldb["Initials"]
    haldb["AuthorList"] = haldb["AuthorList"].apply(_split_list)
    haldb["ArticleType"] = haldb.ArticleType.apply(lambda x: doctype[0][x])
    haldb = haldb[haldb.columns.intersection(alldb.T.columns)]
    haldb.Language = haldb.Language.apply(lambda x: x[0])
    haldb.loc[haldb["Language"] == "en", "Language"] = "eng"
    haldb.loc[haldb["Language"] == "fr", "Language"] = "fre"
    haldb.ArticleTitle = haldb.ArticleTitle.apply(lambda x: x[0])
    haldb.KeywordList = haldb.KeywordList.dropna().apply(lambda x: [{"@MajorTopicYN": "N", "#text": keyword} for keyword in x])
    haldb.MeshHeadingList = haldb.MeshHeadingList.dropna().apply(lambda x: [{"DescriptorName": {"@MajorTopicYN": "N", "#text": mesh}} for mesh in x])
    haldb["dois"] = haldois
    haldb = haldb.set_index("dois")
    haldb = haldb[~haldb.index.isin(intersection)]
    return pd.concat([alldb.T, haldb])


def main() -> None:
    """main func"""
    print("adding HAL")
    doctype = pd.read_json(r"data/doctype.json", orient="index")
    results = get_hal_request(URL, PARAMETERS)
    path = Path("data/resources/")
    merged_db = reformat_respone(results, pd.read_json(path / "all_DB.json"), doctype)
    merged_db[~merged_db.index.duplicated(keep="first")].to_json(path / "all_DB.json", indent=2, orient="index")
