```
(
    medico administrative data.tl
    OU medico administrative database.tl
    OU medico administrative databases.tl
    OU national health fund data.tl
    OU national health fund database.tl
    OU national health fund databases.tl
    OU claims data.tl
    OU claims database.tl
    OU claims databases.tl
    OU national healthcare system.tl
    OU national health data system.tl
    OU national health database.tl
    OU national health databases.tl
    OU national health datasets.tl
    OU national health insurance reimbursement system.tl
    OU snds.tl
    OU pmsi.tl
    OU Programme de Medicalisation des Systemes d'Information.tl
    OU french hospital discharge database.tl
    OU cepidc.tl
    OU dcir.tl
    OU sniiram.tl
    OU sniir am.tl
    OU national health insurance information system.tl
    OU egb database.tl
    OU echantillon generaliste des beneficiaires.tl
    OU french national healthcare.tl
    OU french national healthcare insurance.tl
    OU french national healthcare database.tl
    OU french national healthcare databases.tl
    OU french national health insurance.tl
    OU french national health data.tl
    OU french national health data system.tl
    OU french national hospital database.tl
    OU french national hospital discharge.tl
    OU french health insurance database.tl
    OU french health insurance scheme.tl
    OU french administrative healthcare.tl
    OU french death certificates.tl
    OU systeme national des donnees de sante.tl
    OU national health insurance system database.tl
    OU french health insurance system.tl
    OU french health insurance service.tl
    OU nationwide french.tl
    OU nationwide french registry.tl
    OU national administrative database.tl
    OU french medical-administrative database.tl
    OU information systems medicalization program.tl
)
ET
(
    french.tl
    OU French.tl
    OU france.tl
    OU France.tl
)
ET 2007->3000.an
```