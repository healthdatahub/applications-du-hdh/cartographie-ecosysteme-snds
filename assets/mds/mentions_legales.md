## Objet

La cartographie interactive de l’écosystème SNDS (disponible ici <https://ecosysteme-snds.health-data-hub.fr/>) a pour vocation de mettre à la disposition de la communauté des utilisateurs et personnes intéressées par le Système National des Données de Santé ( « SNDS ») une cartographie interactive sur les utilisateurs et l’écosystème utilisant le SNDS. Cette cartographie interactive est construite par le Health Data Hub à partir d’informations publiques et issues de PubMed (<https://pubmed.ncbi.nlm.nih.gov/>). Tout visiteur connecté à ce site est réputé avoir pris connaissance des mentions légales et conditions d’utilisation ci-après.
​
## Éditeur

Ce site est sous la responsabilité du groupement d’intérêt public Plateforme des données de santé ou « Health Data Hub », dont le siège est situé 9 rue Georges Pitard, 75015 Paris, E-mail : <hdh@health-data-hub.fr>.

Le représentant légal du Health Data Hub et directeur de la publication du site est Mme Stéphanie Combes en sa qualité de Directrice du Health Data Hub.
​
## Hébergeur

Ce site est hébergé sur les serveurs de l'hébergeur OVH, localisés en France.

- Téléphone : +33 (0)8 99 70 17 61
- Site web : www.ovh.com.

## Développement et conception graphique

Le suivi éditorial, graphique et technique du site est assuré au quotidien par les personnels du Health Data Hub et en particulier par l’équipe Data.
​
## Réutilisation des informations publiques

Les conditions de libre réutilisation fixées ci-après s’appliquent uniquement aux contenus, données et publications diffusées sur le site <https://ecosysteme-snds.health-data-hub.fr> et constituent des informations publiques au sens de la loi n°78-753 du 17 juillet 1978.
​
Les marques et logos disponibles sur ce site ne sont pas librement réutilisables. Pour toute demande de réutilisation des marques et logos, veuillez nous contacter à l’adresse suivante : <hdh@health-data-hub.fr>.
​

Le Health Data Hub et les auteurs conservent, respectivement, leurs droits patrimoniaux et moraux sur les publications disponibles sur ce site.

L’utilisateur du site est autorisé :

à reproduire et à rediffuser les informations contenues sur ce site, sous réserve :
de la mention exacte de la source [Auteur, Titre, Éditeur, date de mise à jour, adresse internet] ;
d’informer le Health Data Hub de ces reproductions en contactant <hdh@health-data-hub.fr> ;
à retraiter, à inclure ou à exploiter ces informations, sous réserve de ne pas en dénaturer le sens ni l’exactitude et de ne pas induire en erreur des tiers quant au contenu ou à la source de ces informations ;
pour le monde entier et sans limitation de durée ;
à des fins commerciales ou non.

Pour toute demande de dérogation à ces conditions, veuillez contacter <hdh@health-data-hub.fr>.

## Liens vers ce site

Le Health Data Hub est favorable à la création de liens hypertextes vers les pages de son site internet. De façon générale, tout lien établi doit indiquer de façon claire à l’internaute qu’il est dirigé vers le site <https://ecosysteme-snds.health-data-hub.fr/> , en faisant notamment mention intégrale et visible de cette adresse.
​
Le Health Data Hub se réserve le droit de demander la dissolution des liens dont elle estimera qu’ils sont de nature à porter préjudice à son image ou à ses droits.

## Limitation de responsabilité

### Disponibilité du service

Le site <https://ecosysteme-snds.health-data-hub.fr/> est accessible librement et gratuitement en tout lieu à tout visiteur ayant un accès à internet. Tous les frais pour y accéder (matériel informatique, logiciels, connexion internet, etc.) sont à la charge du visiteur.
Le Health Data Hub s’efforce d’ouvrir l’accès à ce site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure et sous réserve d’éventuelles pannes, modifications et d’interventions de maintenance. Le Health Data Hub peut être amené à interrompre ce site ou une partie des services à tout moment, sans préavis ni indemnités.
​
Le Health Data Hub met en œuvre tous les moyens mis à sa disposition pour assurer un accès et une utilisation de qualité au site et à ses services. L’obligation étant de moyens, la responsabilité du Health Data Hub ne sera pas engagée si ce résultat n’est pas atteint. Le visiteur a la possibilité de contacter le site par messagerie électronique à l’adresse suivante : <opensource@health-data-hub.fr>


### Informations contenues sur ce site

Les informations figurant sur ce site proviennent de sources publiques et considérées comme étant fiables.
​
Toutefois, elles sont susceptibles de contenir des erreurs que le Health Data Hub se réserve le droit de corriger dès qu’elles sont portées à sa connaissance. Les informations disponibles sur ce site sont susceptibles d’être modifiées à tout moment, et peuvent avoir fait l’objet de mises à jour depuis leur dernière consultation.
​
Les sites internet ou informations tierces référencées sur ce site par des liens ont été sélectionnés et mis à disposition des visiteurs à titre d’information. Malgré le soin apporté à leur sélection, ceux-ci peuvent contenir des informations inexactes ou sujettes à interprétation.
​
Le Health Data Hub ne pourra en aucun cas être tenu responsable de tout dommage de quelque nature qu’il soit résultant de l’interprétation ou de l’utilisation des informations disponibles sur ce site ou sur les sites tiers.
