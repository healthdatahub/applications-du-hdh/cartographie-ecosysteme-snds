```SQL
(
    "medico administrative data"
    OR "medico administrative database"
    OR "medico administrative databases"
    OR "national health fund data"
    OR "national health fund database"
    OR "national health fund databases"
    OR "claims data"
    OR "claims database"
    OR "claims databases"
    OR "national healthcare system"
    OR "national health data system"
    OR "national health database"
    OR "national health databases"
    OR "national health datasets"
    OR "national health insurance reimbursement system"
    OR "snds"
    OR "pmsi"
    OR "Programme de Medicalisation des Systemes d'Information"
    OR "french hospital discharge database"
    OR "cepidc"
    OR "dcir"
    OR "sniiram"
    OR "sniir am"
    OR "national health insurance information system"
    OR "egb database"
    OR "echantillon generaliste des beneficiaires"
    OR "french national healthcare"
    OR "french national healthcare insurance"
    OR "french national healthcare database"
    OR "french national healthcare databases"
    OR "french national health insurance"
    OR "french national health data"
    OR "french national health data system"
    OR "french national hospital database"
    OR "french national hospital discharge"
    OR "french health insurance database"
    OR "french health insurance scheme"
    OR "french administrative healthcare"
    OR "french death certificates"
    OR "systeme national des donnees de sante"
    OR "national health insurance system database"
    OR "french health insurance system"
    OR "french health insurance service"
    OR "nationwide french"
    OR "nationwide french registry"
    OR "national administrative database"
    OR "french medical-administrative database"
    OR "information systems medicalization program"
)

PARAMETERS = {
    "fq": '("france" OR "french")&&(submittedDateY_i:[2007 TO 2030])',
    "fl": "*_s,docid",
    "wt": "json",
    "rows": 10000,
}
```