## Contributors and maintenance

Cette application est maintenue par le Health Data Hub. Pour toute requêtes, remarques ou suggestions, vous pouvez creer un Issue sur le [gitlab](https://gitlab.com/healthdatahub/cartographie-ecosysteme-snds/-/issues?sort=created_date&state=opened) ou contacter le Health Data Hub a l'address email suivante <opensource@health-data-hub.fr>

Pour en savoir plus sur la gestion de vos données personnelles éventuellement transmises à
cette occasion et pour exercer vos droits, vous pouvez consulter notre Politique de
confidentialité ou contacter notre Délégué à la protection des données à l'adresse suivante : <dpd@health-data-hub.fr>