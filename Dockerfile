FROM python:3.9.10
COPY . /carto
WORKDIR /carto
RUN pip3 install -r requirements.txt
CMD python3 dashboard.py
